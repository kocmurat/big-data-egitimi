import org.apache.spark.sql.SparkSession

object SparkSqlSelect001 {


  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().appName("Select").master("local[*]").getOrCreate()
    val dataFrames = new DataFrames(spark)
    val userDf = dataFrames.getUserDf()
    val movieDf = dataFrames.getMovieDf()
    val ratingDf = dataFrames.getRatingDf()

    val cinsiyetDf = userDf.select("cinsiyet","id")

    ratingDf.createOrReplaceTempView("ratingstable")

    cinsiyetDf.show()

    val filmadiDf = movieDf.select("filmadi")

    filmadiDf.show()





  }

}
