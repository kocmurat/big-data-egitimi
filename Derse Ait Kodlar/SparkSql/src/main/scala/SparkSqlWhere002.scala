import org.apache.spark.sql.SparkSession

object SparkSqlWhere002 {


  def main(args: Array[String]): Unit = {
   
     val spark = SparkSession.builder().appName("Select").master("local[*]").getOrCreate()
    val userDf=spark.read.parquet("hdfs://localhost:9000/calisma/user/**")
    val movieDf=spark.read.parquet("hdfs://localhost:9000/calisma/movie/**")
    val ratingDf=spark.read.parquet("hdfs://localhost:9000/calisma/rating/")
    
     ratingDf = ratingDf.withColumnRenamed("_0","userid").withColumnRenamed("_1","filmid").withColumnRenamed("_2","puan").withColumnRenamed("_3","zaman")
    userDf = userDf.withColumnRenamed("_0","userid").withColumnRenamed("_1","cinsiyet").withColumnRenamed("_2","yas").withColumnRenamed("_3","meslek").withColumnRenamed("_4","postakodu")
    movieDf = movieDf.withColumnRenamed("_0","movieaid").withColumnRenamed("_1","filmadi").withColumnRenamed("_2","tur")


    userDf.where(userDf("id")===17).show()

    userDf.where(userDf("yas")>45).show()

    userDf.where(userDf("cinsiyet")==="F").select("cinsiyet","yas","meslekkodu").show()


    movieDf.where(movieDf("tur")==="Comedy").show()

    ratingDf.where(ratingDf("userid")===10).show()


  }

}
