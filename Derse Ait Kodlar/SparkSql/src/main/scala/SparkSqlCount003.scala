import org.apache.spark.sql.SparkSession

object SparkSqlCount003 {


  def main(args: Array[String]): Unit = {

    // Count : Sql ifadenizin kaç kayıt döndürdüğünü gösterir
    val spark = SparkSession.builder().appName("Select").master("local[*]").getOrCreate()
    val userDf=spark.read.parquet("hdfs://localhost:9000/calisma/user/**")
    val movieDf=spark.read.parquet("hdfs://localhost:9000/calisma/movie/**")
    val ratingDf=spark.read.parquet("hdfs://localhost:9000/calisma/rating/")
    
    ratingDf = ratingDf.withColumnRenamed("_0","userid").withColumnRenamed("_1","filmid").withColumnRenamed("_2","puan").withColumnRenamed("_3","zaman")
    userDf = userDf.withColumnRenamed("_0","userid").withColumnRenamed("_1","cinsiyet").withColumnRenamed("_2","yas").withColumnRenamed("_3","meslek").withColumnRenamed("_4","postakodu")
    movieDf = movieDf.withColumnRenamed("_0","movieaid").withColumnRenamed("_1","filmadi").withColumnRenamed("_2","tur")
    

    println("CountUserDf = " + userDf.count())
    println("CountMovieDf = " + movieDf.count())
    println("CountRatingsDf = " + ratingDf.count())


    println("Puanı 3 ten büyük olan kayıtların sayısı = " +ratingDf.where(ratingDf("puan")>3).count())
    println("Puanı 3 ve 3 ten küçük olan kayıtların sayısı = " +ratingDf.where(ratingDf("puan")<4).count())


    println("Erkek Kullanıcı Sayısı = " + userDf.where(userDf("cinsiyet")==="M").count())
    println("Bayan Kullanıcı Sayısı = " + userDf.where(userDf("cinsiyet")==="F").count())

    println("Puanı 4 olan Filme Ait Kayıt Sayısı = " + ratingDf.where(ratingDf("puan")===4).count())
    println("Puanı 3 Olan Filme Ati Kayıt Sayısı = " + ratingDf.where(ratingDf("puan")===3).count())
    print("Puanı 4 olan filmlerle puanı 3 olan filmlerin arasındaki fark "+
      (ratingDf.where(ratingDf("puan")===4).count()-ratingDf.where(ratingDf("puan")===3).count()))
  }



}
