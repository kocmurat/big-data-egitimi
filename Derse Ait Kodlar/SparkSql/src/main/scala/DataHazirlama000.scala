import org.apache.spark
import org.apache.spark.sql.SparkSession


object DataHazirlama000 {


  def main(args: Array[String]): Unit = {



    val spark = SparkSession.builder().appName("DataHazırlama").master("local[*]").getOrCreate()
    import  spark.implicits._


    // users datasetenin dataframe çevrilmesi
    val  usersdataset = spark.read.textFile("/home/mk/Desktop/data/users.dat")
    val userRdd = usersdataset.rdd
    val userRdd2 = userRdd.map(x=>parseDataSet(x))
    val userDf = userRdd2.toDF("id","cinsiyet","yas","meslekkodu","postakodu")

    // dataframeleri gösterebilmek için show fonksiyonu kullanılır

    userDf.show()

    // movie datasetinin dataframe çevrilmesi
    val  moviedataset = spark.read.textFile("/home/mk/Desktop/data/movies.dat")
    val  movieRdd = moviedataset.rdd
    val movieRdd2 = movieRdd.map(x=>parseToMovieDataSet(x))
    val movieDf = movieRdd2.toDF("id","filmadi","tur")

    movieDf.show()

    //rating datasetinin dataframe çevrilmesi

    val  ratingDataset = spark.read.textFile("/home/mk/Desktop/data/ratings.dat")
    val ratingRdd = ratingDataset.rdd
    val ratingRdd2 = ratingRdd.map(x=>parseToRatingDataSet(x))
    val ratinfDf = ratingRdd2.toDF("userid","filmid","puan","zaman")
    ratinfDf.show()

  }


  def parseDataSet(satir:String)=
  {
    val tumdata = satir.split("::")
    val id = tumdata(0).toInt
    val cinsiyet = tumdata(1)
    val yas = tumdata(2).toInt
    val meslekkodu = tumdata(3).toInt
    val postakodu= tumdata(4)

    (id,cinsiyet,yas,meslekkodu,postakodu)
  }
  def parseToMovieDataSet(satir:String)=
  {
    val tumdata = satir.split("::")
    val id= tumdata(0).toInt
    val filmAdi= tumdata(1)
    val tur= tumdata(2)


    (id,filmAdi,tur)
  }

  def parseToRatingDataSet(satir:String)=
  {
    val tumdata = satir.split("::")
    val userId= tumdata(0).toInt
    val filmId= tumdata(1).toInt
    val puan= tumdata(2).toInt
    val zaman = tumdata(3)

    (userId,filmId,puan,zaman)
  }

}
