import DataHazirlama000.{parseDataSet, parseToMovieDataSet, parseToRatingDataSet}
import org.apache.spark.sql.{DataFrame, SparkSession}

class DataFrames(spark:SparkSession) extends Serializable {




  import spark.implicits._



  def getUserDf(): DataFrame = {


    val usersdataset = spark.read.textFile("/home/mk/Desktop/data/users.dat")
    val userRdd = usersdataset.rdd // dataset üzerinde map işlemi uygulamak için rdd çevrilmesi gerekiyoır
    val userRdd2 = userRdd.map(x=>parseDataSet(x)) // yeni rdd oluşturduk . ( Datalarımınz :: ya göre parse edildi)
    val userDf = userRdd2.toDF("id", "cinsiyet", "yas", "meslekkodu", "postakodu")
    return userDf

  }

  def getMovieDf(): DataFrame = {


    val moviedataset = spark.read.textFile("/home/mk/Desktop/data/movies.dat")
    val movieRdd = moviedataset.rdd
    val movieRdd2 = movieRdd.map(x => parseToMovieDataSet(x))
    val movieDf = movieRdd2.toDF("id", "filmadi", "tur")
    return movieDf

  }

  def getRatingDf(): DataFrame = {



    val ratingDataset = spark.read.textFile("/home/mk/Desktop/data/ratings.dat")
    val ratingRdd = ratingDataset.rdd
    val ratingRdd2 = ratingRdd.map(x => parseToRatingDataSet(x))
    val ratinfDf = ratingRdd2.toDF("userid", "filmid", "puan", "zaman")
    return ratinfDf

  }


  def parseDataSet(satir: String) = {

    val tumdata = satir.split("::")
    val id = tumdata(0).toInt
    val cinsiyet = tumdata(1)
    val yas = tumdata(2).toInt
    val meslekkodu = tumdata(3).toInt
    val postakodu = tumdata(4)

    (id, cinsiyet, yas, meslekkodu, postakodu)
  }

  def parseToMovieDataSet(satir: String) = {
    val tumdata = satir.split("::")
    val id = tumdata(0).toInt
    val filmAdi = tumdata(1)
    val tur = tumdata(2)


    (id, filmAdi, tur)
  }



  def parseToRatingDataSet(satir: String) = {
    val tumdata = satir.split("::")
    val userId = tumdata(0).toInt
    val filmId = tumdata(1).toInt
    val puan = tumdata(2).toInt
    val zaman = tumdata(3)

    (userId, filmId, puan, zaman)
  }


}
