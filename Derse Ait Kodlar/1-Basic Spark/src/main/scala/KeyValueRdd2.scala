import org.apache.spark.SparkContext

object KeyValueRdd2 {


  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]","KeyValueRdd2")
    val rdd = sc.textFile("/home/mk/Masaüstü/yas.csv")

    val rdd2 = rdd.map(parseLine)



    val rdd3 = rdd2.mapValues(a=>(a,1)).reduceByKey((x,y)=>(x._1+y._1,x._2+y._2))
    val rdd4 = rdd3.sortByKey()
    val rdd5 = rdd4.mapValues(x=>(x._1/x._2))
    rdd5.foreach(println)
  }

  def parseLine(line:String)=
  {
    val fields = line.split(",")
    val yasRdd = fields(1).toInt
    val arkadasRdd = fields(2).toInt
    (yasRdd,arkadasRdd)
  }
}
