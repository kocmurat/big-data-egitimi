import org.apache.spark.SparkContext

object WordCount {


  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]", "RddFilter")
    val rdd = sc.textFile("/home/mk/Masaüstü/ml-100k/u.user")
    val rdd2 = rdd.map(parseLine).filter(x=>x._1=="F")
    val rdd3 =  rdd2.map(x=>(x._2,1))
    val rdd4 = rdd3.reduceByKey((x,y)=>(x+y))


    for(result <- rdd4)
      {
        val job = result._1
        val count = result._2

        println(job + " mesleğini yapan " + count + " kişi bulunmaktadır..")
      }


    /*
    (24,(100,1)) => (x,y) => (x._1 + y_1 , (x._2 + y._2)
    (24,(120,1))
     */

  }

  def parseLine(line: String) = {
    val fields = line.split("\\|")
    val sexRdd = fields(2).toString
    val jobRdd = fields(3).toString
    (sexRdd, jobRdd)
  }
}
