import org.apache.spark.{SparkConf, SparkContext}

object Spark1 {
  def main(args: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("Spark-1").setMaster("local[*]") // Uygulama hakkında bilgi içeren nesne

    val sc = new SparkContext(conf) // sparka ait bir kümeye nasıl erişebileceğiniz söyleyen bir contextdir.

    val rdd = sc.parallelize(List(1,3,5,7,9))


    val rdd2 = rdd.map(al=>al*al) // fonksiyonel programlama buna deniyor.


    rdd.foreach(println)
    println("----------------------------------------------------")
    rdd2.foreach(x=>println(x))





  }


}
