import org.apache.spark.SparkContext

object RddFilter {

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]","RddFilter")
    val rdd = sc.textFile("/home/mk/Masaüstü/ml-100k/u.user")
    val rdd2 = rdd.map(parseLine)
    val rdd3 = rdd2.filter(x=>x._2=="F")
    val rdd4 = rdd2.filter(x=>x._2=="M")

    println("Erkek Sayısı : " + rdd4.count() + " Bayan Sayısı : "+ rdd3.count())
  }
  def parseLine(line:String)=
  {
    val fields = line.split("\\|")
    val ageRdd = fields(1).toInt
    val sexRdd = fields(2).toString
    val jobRdd = fields(3).toString
    (ageRdd,sexRdd,jobRdd)
  }
}
