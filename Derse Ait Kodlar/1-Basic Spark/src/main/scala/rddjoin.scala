import org.apache.spark.SparkContext

object rddjoin {


  def parseLine(line: String) = {
    var fields = line.split("\t")
    var index1 = fields(0).toString
    var index2 = fields(1).toString
    var index3 = fields(2).toString

    (index2,( index1, index3))

  }

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext("local[*]", "Join")
    val rdd = sc.textFile("/home/mk/Masaüstü/ml-100k/kisi")
    val rdd2 = rdd.map(x=>parseLine(x))
    rdd2.foreach(println)


    val rdd5 = sc.textFile("/home/mk/Masaüstü/ml-100k/bilgi")
    val rdd6 = rdd5.map(parseLine)
    rdd6.foreach(println)


    val x = rdd6.join(rdd2)

    x.foreach(println)

  }


}
