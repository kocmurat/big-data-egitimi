import org.apache.spark.SparkContext

object KeyValue {

  def parseLine(line: String) = {
    var fields = line.split("\t")
    var index1 = fields(1).toInt
    var index2 = fields(2).toInt
    (index2, index1)

  }

  def main(args: Array[String]): Unit = {

    val sc = new SparkContext("local[*]", "Key-Value")
    val rdd = sc.textFile("/home/mk/Masaüstü/ml-100k/u.data")
    val rdd2 = rdd.map(x => parseLine(x))
    val rdd5 = rdd2.mapValues(x => (x, 1))
    rdd5.foreach(println)
    var rdd3 = rdd2.mapValues(x => (x, 1)).reduceByKey((x,y)=>(x._1+y._1,x._2+y._2))
    rdd3.foreach(println)
    val rdd4 = rdd3.mapValues(x=>x._1/x._2)
    rdd4.foreach(println)

  }

}
