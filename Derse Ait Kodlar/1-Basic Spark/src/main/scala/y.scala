import org.apache.spark.sql.{DataFrame, SparkSession}

object y {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder().master("local[*]").getOrCreate()

    val x = getDf(spark, "deneme")
    x.select("ad").where(x.col("ad")==="a").show()
  }


  def getDf(spark: SparkSession, tblName: String): DataFrame = {


    val Df = spark.read.format("jdbc")

      .option("url", "jdbc:mysql://localhost:3306/test")
      .option("driver", "com.mysql.jdbc.Driver")
      .option("user", "root")
      .option("dbtable", tblName)
      .option("password", "").load()

    return Df


  }
}
