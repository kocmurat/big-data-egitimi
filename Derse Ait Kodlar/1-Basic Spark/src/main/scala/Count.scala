import org.apache.spark.SparkContext

object Count {

  def main(args: Array[String]): Unit = {


    val sc = new SparkContext("local[*]", "Count")
    val rdd = sc.textFile("/home/mk/Masaüstü/cekilis")
    //val rdd2  = rdd.map(x=>x.split("\t")(1))

    //  split içindeki /t => 1. Tab karakterine  veya 2. olarak space karakterine karşılık gelir


    val rdd3 = rdd.countByValue()
    val rdd4 = rdd3.toSeq.sortBy(_._1)   // sortBy(_._2) => () parantezleri içerisindeki 2. parametreyi küçükten büyüpğe sıralar




    rdd4.foreach(println)    // 1 kere 3 kişi etiketliyenlerden 15 kişiye

    println("-------------------------------------------")


    val rdd6 = rdd4.filter(x =>x._2>1 && x._2 < 3).toSeq.sortBy(_._1)
    rdd6.foreach(println)   // 1 kere 3 kişi etiketliyenlerden 5 kişiye

    println("-------------------------------------------")

    val rdd7 = rdd4.filter(x =>x._2>=3).toSeq.sortBy(_._1)
    rdd7.foreach(println)   // 3 den fazla kere 3 kişi etiketliyenlerden 4 kişiye

  }
}
